# AWS Kinesis

At the time of this writing there are four streams:

* Data Stream
* Data Firehose 
* Data Analytics
* Video Stream

# Data Stream
The Data Stream was the initial Kinesis product.  A data stream contains 1 to 500 shards and is user scalable. 
A shard has the following throughput:
 * 1 MB/sec input
 * 2 MB/sec output

You can write to a data stream using the [aws-sdk](./dataStream.js) and [aws-cli](./dataStream.sh).

# Data Firehose Delivery Stream
A Firehose delivery stream uses a Data Stream under the hood and provides a complete delivery solution.  I believe when a Firhose deilvery stream is created, AWS automatically creates (and scales?) a data stream.
Data sent to the firehose stream can come from the [AWS Agent](https://docs.aws.amazon.com/firehose/latest/dev/writing-with-agents.html#download-install) or can come from the PUT API using the [AWS-CLI](firehoseDeliveryStream.sh) or [aws-sdk](firehoseDeliveryStream.js).

The source data for a Firehose delivery stream can come from a data stream or from the PUT API.

You can write to a Firehose delivery stream by using the [aws-sdk](./firehoseDeliveryStream.js) and [aws-cli](./firehoseDeliveryStream.sh).

# Data Analytics
Data Analytic streams can be sourced from Data Streams and Firehose streams.  A Data Stream can be use to run SQL-like syntax on a stream in realtime.  This can be used to smooth out rough data before it reaches the destination.

# Video Streams
Use to send streaming video content into AWS.

# Notes

https://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/Kinesis.html#putRecords-property
